<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use App\Form\Handler\PostFormHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/blog", name="post_")
 */
class PostController extends AbstractController
{
    protected $entityManagerInterface;
    protected $repository;
    public function __construct(EntityManagerInterface $entityManagerInterface, PostRepository $repository)
    {
        $this->entityManagerInterface = $entityManagerInterface;
        $this->repository = $repository;
    }

    /**
     * @Route("/lists", name="lists")
     */
    public function index(){
        return $this->render('post/index.html.twig', [
            'posts' => $this->repository->findAll(),
        ]);
    }
    
    /**
     * @Route("/new", name="new")
     */
    public function new(Request $request, Security $security): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $formHandler = new PostFormHandler($form, $request, $this->entityManagerInterface, $security);

        if (!is_null($formHandler->process())) {
            $this->addFlash('success','Création article ok');
            return $this->redirectToRoute('home');
        }
        return $this->render('post/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     */
    public function update(Request $request, Post $post, Security $security)
    {
        $form = $this->createForm(PostType::class, $post);
        $formHandler = new PostFormHandler($form, $request, $this->entityManagerInterface, $security);

        if (!is_null($formHandler->process())) {
            $this->addFlash('success','Modifiction ok');
            return $this->redirectToRoute('post_lists');
        }

        return $this->render('post/edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
