<?php


namespace App\Form\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class PostFormHandler
{
    private $form;
    private $request;
    private $entityManagerInterface;
    private $security;

    public function __construct(FormInterface $form, Request $request, EntityManagerInterface $entityManagerInterface, Security $security)
    {
        $this->form = $form;
        $this->request = $request;
        $this->entityManagerInterface = $entityManagerInterface;
        $this->security = $security;
    }

    public function process()
    {
        $this->form->handleRequest($this->request);
        if ($this->form->isSubmitted() && $this->form->isValid()){
            return $this->onSuccess($this->security);
        }
        return null;
    }

    private function onSuccess()
    {
        $post = $this->form->getData();
        $post->setUser($this->security->getUser());
        $this->entityManagerInterface->persist($post);
        $this->entityManagerInterface->flush();
        return $post;
    }
}