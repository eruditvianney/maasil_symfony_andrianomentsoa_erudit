<?php


namespace App\Form\Handler;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationHadler
{
    private $form;
    private $request;
    private $entityManagerInterface;
    private $encoders;

    public function __construct(FormInterface $form, Request $request, EntityManagerInterface $entityManagerInterface, UserPasswordEncoderInterface $encoders)
    {
        $this->form = $form;
        $this->request = $request;
        $this->entityManagerInterface = $entityManagerInterface;
        $this->encoders = $encoders;
    }

    public function process()
    {
        $this->form->handleRequest($this->request);
        if ($this->form->isSubmitted() && $this->form->isValid()){
            $user = $this->onSuccess();
            return $user;
        }
        return null;
    }

    private function onSuccess()
    {

        $user = $this->form->getData();
        $hash = $this->encoders->encodePassword($user, $user->getPassword());
        $user->setPassword($hash);
        $user->setRoles(['ROLE_USER']);
        $this->entityManagerInterface->persist($user);
        $this->entityManagerInterface->flush();
        return $user;
    }
}