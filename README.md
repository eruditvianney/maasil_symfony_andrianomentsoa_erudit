# instruction d'installation du projet

# 1 installation composer
composer install

# 2 modifier la configuration de votre base de données dans le fichier .env
# 3 création de la base de données
php bin/console doctrine:database:create

# 4 création table
php bin/console doctrine:schema:update --force

# 5  lancer le server
php -S 127.0.0.1:3000 -t public

# 6 Ouvrir le projet dans votre navigateur
http://127.0.0.1:3000

# Remarque
Sur la page création auteur, j'ajoute les deux champ email et mot de passe grace à cette phrase

"l’auteur d’un article serait directement l’utilisateur connecté."